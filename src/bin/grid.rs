#![feature(test)]

extern crate test;

use std::collections::HashMap;
use std::hash::{Hash, Hasher};

// A 2D grid
//         y
//    +----> n_cols
//    \
//    \
// x  v
//    n_rows
#[derive(Debug, Clone, Eq)]
pub struct Grid {
    n_rows: u64,
    n_cols: u64,
}

impl Grid {
    fn new(n_rows: u64, n_cols: u64) -> Self {
        Self { n_rows, n_cols }
    }
    fn move_right(&self) -> Self {
        Grid::new(self.n_rows, self.n_cols - 1)
    }
    fn move_down(&self) -> Self {
        Grid::new(self.n_rows - 1, self.n_cols)
    }
    fn is_size_one(&self) -> bool {
        self.n_rows == 1 && self.n_cols == 1
    }
    fn is_size_zero(&self) -> bool {
        self.n_rows == 0 || self.n_cols == 0
    }
}

impl PartialEq for Grid {
    fn eq(&self, grid: &Grid) -> bool {
        // Leverage problem symmetry: solution for (m, n) == solution for (n, m)
        (self.n_rows == grid.n_rows && self.n_cols == grid.n_cols)
            || (self.n_rows == grid.n_cols && self.n_cols == grid.n_rows)
    }
}

impl Hash for Grid {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // Leverage problem symmetry: solution for (m, n) == solution for (n, m)
        if self.n_rows >= self.n_cols {
            self.n_rows.hash(state);
            self.n_cols.hash(state);
        } else {
            self.n_cols.hash(state);
            self.n_rows.hash(state);
        }
    }
}

pub fn recurse(n_rows: u64, n_cols: u64) -> u64 {
    _recurse(Grid::new(n_rows, n_cols))
}

fn _recurse(grid: Grid) -> u64 {
    if grid.is_size_zero() {
        return 0;
    } else if grid.is_size_one() {
        return 1;
    }
    _recurse(grid.move_right()) + _recurse(grid.move_down())
}

pub fn memo(n_rows: u64, n_cols: u64) -> u64 {
    let mut cache: HashMap<Grid, u64> = HashMap::new();
    _memo(Grid::new(n_rows, n_cols), &mut cache)
}

fn _memo(grid: Grid, cache: &mut HashMap<Grid, u64>) -> u64 {
    if let Some(n_sol) = cache.get(&grid) {
        return *n_sol;
    } else if grid.is_size_zero() {
        return 0;
    } else if grid.is_size_one() {
        return 1;
    }
    let n_sol = _memo(grid.move_right(), cache) + _memo(grid.move_down(), cache);
    cache.insert(grid, n_sol);
    n_sol
}

fn main() {
    println!("Found {} solutions", memo(15, 15));
    println!("Found {} solutions", recurse(15, 15));
}

#[cfg(test)]
mod tests {
    use crate as grid;
    use test::Bencher;

    #[test]
    fn recurse() {
        assert_eq!(grid::recurse(1, 0), 0);
        assert_eq!(grid::recurse(1, 1), 1);
        assert_eq!(grid::recurse(1, 10), 1);
        assert_eq!(grid::recurse(2, 2), 2);
        assert_eq!(grid::recurse(3, 2), 3);
        assert_eq!(grid::recurse(2, 3), 3);
        assert_eq!(grid::recurse(3, 3), 6);
        assert_eq!(grid::recurse(4, 4), 20);
    }

    #[test]
    fn memo() {
        assert_eq!(grid::memo(1, 0), 0);
        assert_eq!(grid::memo(1, 1), 1);
        assert_eq!(grid::memo(1, 10), 1);
        assert_eq!(grid::memo(2, 2), 2);
        assert_eq!(grid::memo(3, 2), 3);
        assert_eq!(grid::memo(2, 3), 3);
        assert_eq!(grid::memo(3, 3), 6);
        assert_eq!(grid::memo(4, 4), 20);
    }

    #[bench]
    fn bench_recurse(bencher: &mut Bencher) {
        bencher.iter(|| {
            // Weird stuff to prevent the compiler from ignoring the code
            // https://doc.rust-lang.org/nightly/unstable-book/library-features/test.html
            let n = test::black_box(10);
            grid::recurse(n, n)
        });
    }

    #[bench]
    fn bench_memo(bencher: &mut Bencher) {
        bencher.iter(|| grid::memo(10, 10));
    }
}

use std::collections::HashMap;

// Recurses to compute the previous values
pub fn recurse(n: u64) -> u64 {
    match n {
        0 => 0,
        1 => 1,
        2 => 1,
        n => recurse(n - 1) + recurse(n - 2),
    }
}

// Uses memoization to cache computed values
pub fn memo(n: u64) -> u64 {
    let mut cache = HashMap::from([(0, 0), (1, 1), (2, 1)]);
    _memo(n, &mut cache)
}

fn _memo(n: u64, cache: &mut HashMap<u64, u64>) -> u64 {
    match cache.get(&n) {
        Some(value) => *value,
        None => {
            let value = _memo(n - 1, cache) + _memo(n - 2, cache);
            cache.insert(n, value);
            value
        }
    }
}

// We don't need to memoize everything: only the last and previous last values
// have to be stored
pub fn memo_lite(n: u64) -> u64 {
    _memo_lite(n)[0]
}

fn _memo_lite(n: u64) -> [u64; 2] {
    match n {
        0 => [0, 0],
        1 => [1, 0],
        2 => [1, 1],
        n => {
            let [value, prev_value] = _memo_lite(n - 1);
            [value + prev_value, value]
        }
    }
}

fn main() {
    use std::time::Instant;
    struct Case {
        name: String,
        func: fn(u64) -> u64,
    }
    for case in vec![
        Case {
            name: "recurse".to_owned(),
            func: recurse,
        },
        Case {
            name: "memo".to_owned(),
            func: memo,
        },
        Case {
            name: "memo_lite".to_owned(),
            func: memo_lite,
        },
    ] {
        let now = Instant::now();
        let result: u64;
        {
            // case.func(...) is parsed as a function call, need extra parens or create a variable
            result = (case.func)(40);
        }
        let elapsed = now.elapsed();
        println!("{}(40)\t\t= {} in {:.2?}", case.name, result, elapsed);
    }
    println!("Expected\t\t= 102334155");
}

#[cfg(test)]
mod tests {
    use crate as fib;

    #[test]
    fn recurse() {
        assert_eq!(fib::recurse(3), 2);
        assert_eq!(fib::recurse(10), 55);
        assert_eq!(fib::recurse(21), 10946);
    }

    #[test]
    fn memo() {
        assert_eq!(fib::memo(3), 2);
        assert_eq!(fib::memo(10), 55);
        assert_eq!(fib::memo(21), 10946);
    }

    #[test]
    fn memo_lite() {
        assert_eq!(fib::memo_lite(3), 2);
        assert_eq!(fib::memo_lite(10), 55);
        assert_eq!(fib::memo_lite(21), 10946);
    }
}

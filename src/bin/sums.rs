#![feature(test)]

extern crate test;

use std::collections::HashMap;

fn recurse(target: u64, mut candidates: Vec<u64>) -> bool {
    candidates = candidates
        .into_iter()
        .filter(|candidate| *candidate > 0)
        .collect();
    _recurse(target, &candidates)
}

fn _recurse(target: u64, candidates: &[u64]) -> bool {
    if target == 0 {
        return true;
    }
    for candidate in candidates {
        if target >= *candidate && _recurse(target - candidate, candidates) {
            return true;
        }
    }
    false
}

// Only makes sense for false cases
fn memo(target: u64, mut candidates: Vec<u64>) -> bool {
    // Filter 0s from candidates, they won't affect the solution
    candidates = candidates
        .into_iter()
        .filter(|candidate| *candidate > 0)
        .collect();
    let mut cache = HashMap::from([(0, true)]);
    _memo(target, &candidates, &mut cache)
}

fn _memo(target: u64, candidates: &[u64], cache: &mut HashMap<u64, bool>) -> bool {
    if let Some(result) = cache.get(&target) {
        return *result;
    }
    for candidate in candidates {
        // Prevent overflows
        if target >= *candidate && _memo(target - candidate, candidates, cache) {
            cache.insert(target, true);
            return true;
        }
    }
    cache.insert(target, false);
    false
}

fn main() {
    println!("{}", recurse(255, vec![7, 14]));
    println!("{}", memo(255, vec![7, 14]));
}

#[cfg(test)]
mod tests {
    use crate as sums;
    use test::Bencher;

    #[test]
    fn recurse() {
        // 3, 2, 2 or 3, 2, 1, 1, or ...
        assert_eq!(sums::recurse(7, vec![1, 2, 3]), true);
        assert_eq!(sums::recurse(7, vec![0, 7]), true);
        assert_eq!(sums::recurse(7, vec![7]), true);
        assert_eq!(sums::recurse(7, vec![0, 8]), false);
        assert_eq!(sums::recurse(7, vec![5, 7, 8, 9, 10, 2]), true);
        assert_eq!(sums::memo(100, vec![5, 7, 8, 9, 10, 2]), true);
    }

    #[test]
    fn memo() {
        // 3, 2, 2 or 3, 2, 1, 1, or ...
        assert_eq!(sums::memo(7, vec![1, 2, 3]), true);
        assert_eq!(sums::memo(7, vec![0, 7]), true);
        assert_eq!(sums::memo(7, vec![7]), true);
        assert_eq!(sums::memo(7, vec![0, 8]), false);
        assert_eq!(sums::memo(7, vec![5, 7, 8, 9, 10, 2]), true);
        assert_eq!(sums::memo(100, vec![5, 7, 8, 9, 10, 2]), true);
    }

    #[bench]
    fn bench_recurse(bencher: &mut Bencher) {
        bencher.iter(|| {
            // This weird thing again
            let n = test::black_box(7);
            sums::recurse(155, vec![n, 14])
        });
    }

    #[bench]
    fn bench_memo(bencher: &mut Bencher) {
        bencher.iter(|| sums::memo(155, vec![7, 14]));
    }
}

# dynpro
A collection of dynamic programming problems solved in Rust

**NOTE:** to use `#[bench]`, the nightly compiler must be used:
```bash
cargo +nightly bench --bin <bin>
```
